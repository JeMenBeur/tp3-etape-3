package fr.siovhb.velostar;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class StationVeloTest {
    @Test
    public void creerStationTest() {
        StationVelo uneStation;
        uneStation = new StationVelo(1, "Villejean", 15, 20, 0, 0);
        assertNotNull(uneStation);
        assertEquals("Villejean", uneStation.getNom());
    }
    @Test
    public void getNbVelosDisponiblesTest() {
        StationVelo uneStation;
        uneStation = new StationVelo(2, "John Kennedy", 20, 25, 0, 0);
        assertNotNull(uneStation);
        assertEquals(25, uneStation.getNbVelosDisponibles());
    }
    @Test
    public void getNbAttachesDisponiblesTest() {
        StationVelo uneStation;
        uneStation = new StationVelo(2, "John Kennedy", 20, 25,0 ,0 );
        assertNotNull(uneStation);
        assertEquals(20, uneStation.getNbAttachesDisponibles());
    }

    @Test
    public void getLongitudeTest() {
        StationVelo uneStation;
        uneStation = new StationVelo(2, "John Kennedy", 20, 25,0 ,0 );
        assertNotNull(uneStation);
        assertEquals(0,0, uneStation.getLongitude());
    }

    @Test
    public void getLatitudeTest() {
        StationVelo uneStation;
        uneStation = new StationVelo(2, "John Kennedy", 20, 25,0 ,0 );
        assertNotNull(uneStation);
        assertEquals(0,0, uneStation.getLatitude());
    }

    @Test
    public void getStationVelo() {
        StationVelo uneStation;
        uneStation = new StationVelo(3, "Test", 10, 15,0 ,0 );
        assertNotNull(uneStation);
        assertEquals("3-Test-10-15-0.0-0.0", uneStation.toString());
    }
}