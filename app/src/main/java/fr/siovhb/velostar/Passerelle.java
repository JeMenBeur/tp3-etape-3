package fr.siovhb.velostar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.HttpURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

import android.location.Location;
import android.util.Log;
import java.util.ArrayList;
/**
 * Cette classe fait le lien entre l'application et l'API Rest
 * Elle utilise les classes JSONObject et JSONArray pour parcourir la chaîne au format JSON
 */
public class Passerelle {

    /** Membres privés */
    private static String _urlStationVelos = "https://data.explore.star.fr/api/records/1.0/search/?dataset=vls-stations-etat-tr&rows=100";

    /**
     * Fournit une liste de StationVelo à partir des données fournies par l'API Rest de VeloStar
     * @param uneChaineUri URI à invoquer
     * @return ArrayList<StationVelo>
     * @throws Exception
     */
    private static ArrayList<StationVelo> getLesStationsVelosFromUrl(String uneChaineUri) throws Exception {
        JSONObject unObjetJSON;
        ArrayList<StationVelo> lesStationVelos;
        // initialisation d'une liste de StationVelos
        lesStationVelos = new ArrayList<StationVelo>();
        try
        {
            unObjetJSON = loadResultJSON(uneChaineUri);

            // récupération du tableau JSON nommé records
            JSONArray arrayStationVelos = unObjetJSON.getJSONArray("records");

			/* Exemple de données obtenues pour une StationVelo :
               "fields": {
                    "etat": "En fonctionnement",
                    "lastupdate": "2017-03-19T00:13:03+00:00",
                    "nombrevelosdisponibles": 15,
                    "nombreemplacementsactuels": 30,
                    "nom": "République",
                    "nombreemplacementsdisponibles": 15,
                    "idStationVelo": 1,
                    "coordonnees": [
                    48.1100259201,
                    -1.6780371631
                    ]
               }
            */

            // parcours du tableau json
            for (int i = 0 ; i < arrayStationVelos.length() ; i++)
            {	// création de l'élement courant à chaque tour de boucle
                JSONObject courant = arrayStationVelos.getJSONObject(i).getJSONObject("fields");
                StationVelo uneStationVelo;
                // lecture des propriétés
                uneStationVelo = JsonObjectFieldToStationVelo(courant);
                lesStationVelos.add(uneStationVelo);
            }
            return lesStationVelos;		// retourne la liste des stations de vélos
        }
        catch (Exception ex)
        {	Log.e("Passerelle", "Exception : " + ex.toString());
            throw (ex);
        }
    }

    private static StationVelo JsonObjectFieldToStationVelo(JSONObject field) throws Exception{
        StationVelo uneStationVelo;
        // lecture des propriétés
        int id = field.getInt("idstation");
        String name = field.getString("nom");
        int bikesavailable = field.getInt("nombrevelosdisponibles");
        int slots = field.getInt("nombreemplacementsactuels");
        double latitude = field.getJSONArray("coordonnees").getDouble(0);
        double longitude = field.getJSONArray("coordonnees").getDouble(1);
        int nbEmplacementsActuels = slots - bikesavailable;

        // ajoute la StationVelo à la collection
        uneStationVelo = new StationVelo(id, name, nbEmplacementsActuels , bikesavailable, longitude, latitude);
        return uneStationVelo;
    }


    /**
     * Fournit le flux XML reçu suite à l'appel du service web localisé à l'uri spécifié
     * @param unURL
     * @return Document
     * @throws Exception
     */
    private static JSONObject loadResultJSON(String unURL) throws Exception {
        JSONObject unJsonObject = null;
        String leResultat;

        URL url = new URL(unURL);
        // établissement d'une requête GET pour demander le jeu de données exprimé dans unURL
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        // exécution de la requête récupération de la réponse dans un flux en lecture (InputStream)
        InputStream unFluxEnEntree = con.getInputStream();

        // conversion du flux en chaîne
        leResultat = convertStreamToString(unFluxEnEntree);

        // transformation de la chaîne en objet JSON
        JSONObject json = new JSONObject(leResultat);
        return json;
    }

    /**
     * Lecture de l'intégralité du contenu à partir du flux en entrée is
     * @param is  flux d'entrée
     * @return string chaîne lue sur le fluc d'entrée
     */
    private static String convertStreamToString(InputStream is) throws Exception {
        // instanciation d'un flux bufferisé sur le flux en entrée
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        // instanciation d'un constructeur de chaîne pour mettre bout à bout les caractères reçus 
        StringBuilder sb = new StringBuilder();
        String line = null;

        // lecture du flux d'entrée ligne à ligne
        line = reader.readLine();
        while (line != null) {
            sb.append(line + "\n");
            line = reader.readLine();
        }
        is.close();  // fermeture du flux en entrée
        return sb.toString();
    }
    /**
     * Fournit une liste de StationVelos à partir des données fournies par l'API Rest de VeloStar
     * @return ArrayList<StationVelo>
     * @throws Exception
     */
    public static ArrayList<StationVelo> getLesStationsVelos() throws Exception {
        return getLesStationsVelosFromUrl(_urlStationVelos);
    }
}
