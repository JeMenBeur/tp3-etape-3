package fr.siovhb.velostar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView;

import java.util.ArrayList;

public class  MainActivity extends AppCompatActivity {
    ListView listViewStations;
    ArrayList<StationVelo> lesDonnees;
    StationVeloAdapter adaptateur;
    private EditText editTextNom;
    private Button buttonAjouter;

    /**
     * Méthode appelée lors de la création de l'activité
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stationvelos_main);
        initialisations();
        // récupération de la liste de stations de vélos
        // demande d'envoi de la requête API dans un thread séparé
        AsyncTaskStationVelos unThread;
        unThread = new AsyncTaskStationVelos();
        unThread.execute();
    }
    /**
     * Initialise les attributs privés référençant les widgets de l'interface utilisateur
     * Instancie les écouteurs et les affecte sur les objets souhaités
     */
    private void initialisations() {
        // instanciation de la source de données, ici 4 chaînes dans une collection de chaînes
        this.lesDonnees = new ArrayList<StationVelo>();
//        this.lesDonnees.add(new StationVelo( 0, "Champs libres", 10, 15, 48.105537, -1.674328));
//        this.lesDonnees.add(new StationVelo( 1, "Marbeuf", 10, 15,48.111749, -1.702077));
//        this.lesDonnees.add(new StationVelo( 2, "Painlevé", 10, 15,48.122994, -1.660547 ));
//        this.lesDonnees.add(new StationVelo( 3, "La Poterie", 10, 15,48.087282, -1.644121 ));

        // on récupère l'id du composant ListView
        this.listViewStations = (ListView) this.findViewById(R.id.listViewStations);
        // on crée l'adaptateur en lui indiquant l'activité, le layout et la source de données
        this.adaptateur = new StationVeloAdapter(this, R.layout.stationvelo_list_item, this.lesDonnees);
        // on associe l'adaptateur au composant ListView
        this.listViewStations.setAdapter(adaptateur);

        // on traite le clic court sur les éléments de la liste
        this.listViewStations.setOnItemClickListener(new ItemOnClick());
        this.listViewStations.setOnItemLongClickListener(new ItemOnLongClick());

        // on récupère l'id de la zone d'édition d'un système
        this.editTextNom = (EditText) this.findViewById(R.id.editTextSysteme);
        this.editTextNom.addTextChangedListener(new editTextChangedListener());

        // on récupère l'id du bouton Ajouter
        this.buttonAjouter = (Button) this.findViewById(R.id.buttonAjouter);
        this.buttonAjouter.setVisibility(View.GONE);
        this.buttonAjouter.setOnClickListener(new BoutonAjouterOnClick());
    }

    //region classes écouteurs
    /**
     * Classe interne servant d'écouteur de l'événement click sur les éléments de la liste
     */
    private class ItemOnClick implements AdapterView.OnItemClickListener{
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
        {
            Toast.makeText(MainActivity.this, lesDonnees.get(position).getNom(), Toast.LENGTH_LONG).show();
            Intent uneIntention ;
            //uneIntention=new Intent(Intent.ACTION_VIEW, Uri.parse("https://data.explore.star.fr/page/home/"));
            //startActivity(uneIntention) ;
            //uneIntention = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:0782390589")) ;
            //startActivity(uneIntention) ;
            //String location = "geo:48.126212,-1.680891";
            String Longitude = (Double.toString(lesDonnees.get(position).getLongitude()));
            String Latitude = (Double.toString(lesDonnees.get(position).getLatitude()));
            String location = "geo:" + Latitude + "," + Longitude;
            uneIntention = new Intent(Intent.ACTION_VIEW, Uri.parse(location)) ;
            startActivity(uneIntention);
        }
    }

    /**
     * Classe interne servant d'écouteur de l'événement click sur le bouton Ajouter
     */
    private class BoutonAjouterOnClick implements View.OnClickListener {
        public void onClick(View v) {
            // on récupère la donnée à partir d'une zone editable txtDonnee
            String laDonnee = editTextNom.getText().toString();
            /*StationVelo laPoterie = new StationVelo( lesDonnees.size(), laDonnee, 10, 15);
            lesDonnees.add(laPoterie);*/
            lesDonnees.add(new StationVelo(lesDonnees.size(), laDonnee, 10, 15, 0, 0));
            // on prévient l'adaptateur que la source de données a changé
            adaptateur.notifyDataSetChanged();
        }
    }
    //endregion

    private class editTextChangedListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (editTextNom.length() != 0){
                buttonAjouter.setVisibility(View.VISIBLE);
            }
            else{
                buttonAjouter.setVisibility(View.GONE);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    private class ItemOnLongClick implements AdapterView.OnItemLongClickListener{
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            lesDonnees.remove(position);
            adaptateur.notifyDataSetChanged();
            return true;
        }
    }

    /**
     * Classe interne servant d'adaptateur aux stations de vélo
     */
    private class StationVeloAdapter extends ArrayAdapter<StationVelo> {
        private final Context context;
        private final ArrayList<StationVelo> values;
        private final int resource;

        public StationVeloAdapter(Context context, int resource, ArrayList<StationVelo> values) {
            super(context,resource,values);
            this.resource =resource;
            this.context =context;
            this.values =values;
        }
        /**
         * Redéfinit la méthode getView qui construit la vue pour l'élément situé à
         * la position spécifiée
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater;
            View rowView;
            TextView textViewStationNom, textViewStationId, textViewStationNbVelosDispos, textViewStationNbAttachesDispos;
            StationVelo uneStationVelo;

            // demande d'obtention d'un désérialisateur de layout xml,
            // càd un objet qui sait transformer un fichier xml en objet View
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // demande à désérialiser le fichier xml identifié par l'id de ressource en objet View
            // rowView est alors un objet regroupant les vues définies dans le layout d'une ligne
            rowView = inflater.inflate(this.resource, parent, false);
            // récupère chaque widget du layout d'un élément
            textViewStationNom = (TextView) rowView.findViewById(R.id.textViewNom);
            textViewStationId = (TextView) rowView.findViewById(R.id.textViewId);
            textViewStationNbVelosDispos = (TextView) rowView.findViewById(R.id.textViewNbVelosDispos);
            textViewStationNbAttachesDispos = (TextView) rowView.findViewById(R.id.textViewNbAttachesDispos);

            // affecte le contenu des widgets d'après le contenu de l'élément reçu
            uneStationVelo = values.get(position);
            textViewStationId.setText(Integer.toString(uneStationVelo.getId()));
            textViewStationNom.setText(uneStationVelo.getNom());
            textViewStationNbVelosDispos.setText(Integer.toString(uneStationVelo.getNbVelosDisponibles()));
            textViewStationNbAttachesDispos.setText(Integer.toString(uneStationVelo.getNbAttachesDisponibles()));
            //textViewStationLongitude.setText(Double.toString(uneStationVelo.getLongitude()));
            //textViewStationLatitude.setText(Double.toString(uneStationVelo.getLatitude()));


            return rowView; // renvoie le groupe de vues ainsi construit
        }
    }


    //region classe AsyncTask
    /**
     * Classe interne pour définir le thread séparé
     */
    private class AsyncTaskStationVelos extends AsyncTask<Void, Void, Object> {
        /*
         * Définit le traitement à faire en arrière-plan
         * Récupère la liste des StationVelos du district demandé ou une exception
         */
        @Override
        public Object doInBackground (Void... inutilise){
            Object leResultat;
            try {
                leResultat = Passerelle.getLesStationsVelos();
            }
            catch (Exception uneException) {
                Log.e("MainActivity", getText(R.string.erreurStationsVelos).toString());
                Log.e("MainActivity", uneException.toString());
                leResultat = uneException;
            }
            return leResultat;
        }
        /**
         * Définit le traitement à faire une fois le traitement en arrière-plan terminé
         * dont les initialisations des composants graphiques
         */
        @Override
        protected void onPostExecute (Object result) {
            if ( result instanceof Exception ) {
                Toast.makeText(MainActivity.this, getText(R.string.erreurStationsVelos)
                        + " " + result.toString(), Toast.LENGTH_LONG).show();
            }
            else {
                adaptateur.clear();
                adaptateur.addAll((ArrayList<StationVelo>)result);
                adaptateur.notifyDataSetChanged();
            }
        }
    }


}