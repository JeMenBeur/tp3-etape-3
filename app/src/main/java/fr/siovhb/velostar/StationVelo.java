package fr.siovhb.velostar;
/**
 * Cette classe décrit une station de VéloStar
 *
 */
public class StationVelo {
	/** Membres privés */
	private int _id;					// id de la station
	private String _nom;				// nom de la station
	private int _nbAttachesDisponibles;	// nombre d'attaches disponibles
	private int _nbVelosDisponibles;	// nombre de vélos disponibles
	private double _longitude;
	private double _latitude;

	/**
	 * Initialise une instance de station
	 * @param id
	 * @param nom
	 * @param nbAttachesDisponibles
	 * @param nbVelosDisponibles
	 * @param _longitude
	 * @param _latitude
	 */
	public StationVelo(int id, String nom, int nbAttachesDisponibles, int nbVelosDisponibles, double _longitude, double _latitude)
	{	this._id = id;
		this._nom = nom.trim();
		this._nbAttachesDisponibles = nbAttachesDisponibles;
		this._nbVelosDisponibles = nbVelosDisponibles;
		this._longitude = _longitude;
		this._latitude = _latitude;
	}
	/** Accesseurs */
	public int getId()
	{	return _id;
	}
	public String getNom() {
		return _nom;
	}
	public int getNbAttachesDisponibles() {
		return _nbAttachesDisponibles;
	}
	public int getNbVelosDisponibles() {
		return _nbVelosDisponibles;
	}
	public double getLongitude(){return _longitude; }
	public double getLatitude(){return _latitude; }

	@Override
	public String toString(){
		String chaine = _id + "-" + _nom + "-" + _nbAttachesDisponibles + "-" + _nbVelosDisponibles + "-" + _longitude + "-" + _latitude;
		return chaine;
	}
}
