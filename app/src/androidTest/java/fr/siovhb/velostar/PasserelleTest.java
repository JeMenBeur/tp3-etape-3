package fr.siovhb.velostar;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
import java.util.ArrayList;
/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class PasserelleTest {
	@Test
	public void testGetLesStations() throws Exception {
		ArrayList<StationVelo> lesStations;
		lesStations = Passerelle.getLesStationsVelos();
		assertNotNull(lesStations);
		assertEquals(55, lesStations.size());
		assertEquals("Musée Beaux-Arts", lesStations.get(0).getNom());
		assertEquals(5510, lesStations.get(0).getId());
		assertEquals(48.109601, lesStations.get(0).getLatitude(), 0);
		assertEquals(-1.67408, lesStations.get(0).getLongitude(), 0);
		assertEquals("Champs Manceaux", lesStations.get(54).getNom());
		assertEquals(5569, lesStations.get(54).getId());
		assertEquals(48.091114, lesStations.get(54).getLatitude(), 0);
		assertEquals(-1.682284, lesStations.get(54).getLongitude(), 0);
	}
}
